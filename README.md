# Fine Weather

⭐ 好天气，好生活！ ⭐

## 🤔 这是什么

这是一个实时气象台，基于 [OpenWeatherMap](https://openweathermap.org/) 的气象数据 API。

[访问 Fine Weather](https://fineweather.soraharu.com/)

## ⚙️ 部署至 Vercel

1. 克隆本仓库到 [GitHub.com](https://github.com/) 或 [GitLab.com](https://gitlab.com/) 个人仓库
2. 在 [Vercel](https://vercel.com/) 新建项目到你的个人仓库
3. 设定 `FRAMEWORK PRESET` 为 `Vue.js`（一般为自动识别）
4. 添加环境变量 `API_KEY`，内容为你在 [OpenWeatherMap](https://openweathermap.org/) 申请的 API key
5. 等待自动部署完成

## 📜 开源许可

基于 [MIT License](https://choosealicense.com/licenses/mit/) 许可进行开源。
